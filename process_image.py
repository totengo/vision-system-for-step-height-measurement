#TODO: deal with the case of faces that are not convex
import numpy as np
import math
import cv2
#from grafo import *
#from exceptions import *

class NoAreasException(Exception):
    pass

class Grafo:
    def __init__(self, n=0, connections=None):
        self.n = n
        self.adj = [[set(), {}, []] for i in range(n)]
        if connections!=None:
            for c, i in zip(connections, range(len(connections))):
                self.adj[i][0] = c
    
    def addEdge(self, node1, node2, data=[]):
        self.adj[node1][0].add(node2)
        self.adj[node2][0].add(node1)
        if len(data)!=0:
            self.adj[node1][1][node2] = data
            self.adj[node2][1][node1] = data
    
    def removeEdge(self, node1, node2):
        if node1 in self.adj[node2][0]:
            self.adj[node1][0].remove(node2)
            self.adj[node2][0].remove(node1)
            if node1 in self.adj[node2][1]:
                self.adj[node1][1].pop(node2)
                self.adj[node2][1].pop(node1)
    
    def getEdgeData(self, node1, node2):
        if node1 in self.adj[node2][0] and node1 in self.adj[node2][1]:
            return self.adj[node1][1][node2]
        else:
            return None
    
    def areConnected(self, node1, node2):
        if node1 in self.adj[node2][0]:
            return True
        else:
            return False
    
    def getNodeSet(self, node):
        return self.adj[node][0]

#ang_tol = 0.0001
#pxpm = 1
##exterior parameters:
#n=20
#delta_d = 35

ang_tol = 0.0001
pxpm = 800/0.01
#exterior parameters:
n=0.01
delta_d = 1

def quad_area(quad):
    tri1 = [quad[0], quad[1], quad[3]]
    tri2 = [quad[0], quad[2], quad[3]]
    measures1 = [math.sqrt(np.dot(tri1[0]-tri1[1], tri1[0]-tri1[1])),
                 math.sqrt(np.dot(tri1[1]-tri1[2], tri1[1]-tri1[2])),
                 math.sqrt(np.dot(tri1[2]-tri1[0], tri1[2]-tri1[0]))]
    measures2 = [math.sqrt(np.dot(tri2[0]-tri2[1], tri2[0]-tri2[1])),
                 math.sqrt(np.dot(tri2[1]-tri2[2], tri2[1]-tri2[2])),
                 math.sqrt(np.dot(tri2[2]-tri2[0], tri2[2]-tri2[0]))]
    s1 = 0.5*np.sum(measures1)
    s2 = 0.5*np.sum(measures2)
    area1 = math.sqrt((s1-measures1[0])*(s1-measures1[1])*(s1-measures1[2]))
    area2 = math.sqrt((s2-measures2[0])*(s2-measures2[1])*(s2-measures2[2]))
    return area1+area2

def is_convex(polygon):
    r1 = polygon[1]-polygon[0]
    r2 = polygon[3]-polygon[1]
    r3 = polygon[2]-polygon[3]
    r4 = polygon[0]-polygon[2]
    lst = [np.sign(r1[0]*r2[1]-r1[1]*r2[0]),
           np.sign(r2[0]*r3[1]-r2[1]*r3[0]),
           np.sign(r3[0]*r4[1]-r3[1]*r4[0]),
           np.sign(r4[0]*r1[1]-r4[1]*r1[0])]
    return(True if len(set(lst))==1 else False)

imgs = [cv2.imread("Initialimage.png"), cv2.imread("FinalImage.png")]
#imgs = [cv2.imread("first_simulated_image_0.png"), cv2.imread("first_simulated_image_1.png")]
grays = [cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) for img in imgs]
#blurred = [cv2.GaussianBlur(img, (3,3), sigmaX = 0, sigmaY = 0) for img in grays]
#edges = [cv2.Sobel(src = img, ddepth = cv2.CV_8UC1, dx=1, dy=1, ksize = 5) for img in blurred]

edges = [cv2.Canny(img, 50, 150, apertureSize = 3) for img in grays]

#lines = [cv2.HoughLines(img, 1, np.pi/180, 95) for img in edges]

lines = [cv2.HoughLines(img, 1, np.pi/180, 60) for img in edges]

pontos = [[], []]

quads = []

midlines = []

#In each element of lines I have the pairs r and theta that determine the line
#I use these pairs to calculate intersections and so forth
#With the intersections I determine the principal face and measure the height of the step
 
#print(lines[0].shape)

#print(imgs[0].shape)

x0, y0, x1, y1 = 0, 0, 0, 0

for k in range(2):
    for linhas in lines[k]:
        r = linhas[0][0]
        theta = linhas[0][1]*180.0/np.pi
        if np.abs(theta-90)<=ang_tol or np.abs(theta-270)<=ang_tol:
            y0 = int(r)
            y1 = int(y0)
            x0 = int(0)
            x1 = int(imgs[k].shape[0])
        elif np.abs(theta-0)<=ang_tol or np.abs(theta-180)<=ang_tol:
            x0 = int(r)
            x1 = int(x0)
            y0 = int(0)
            y1 = int(imgs[k].shape[1])
        else:
            print("oblíquo")
            theta = linhas[0][1]
            margin_points = [(0, r/np.sin(theta)), (r/np.cos(theta), 0),
                             (imgs[k].shape[0], r/np.sin(theta)-imgs[k].shape[0]*np.cos(theta)/np.sin(theta)),
                             (r/np.cos(theta)-imgs[k].shape[1]*np.sin(theta)/np.cos(theta), imgs[k].shape[1])]
            valid_points = []
            for vp in margin_points:
                if vp[0]>=0 and vp[0]<=imgs[k].shape[0] and vp[1]>=0 and vp[1]<=imgs[k].shape[1]:
                    valid_points.append(vp)
                    if len(valid_points)>=2:
                        break
            if len(valid_points)<2:
                raise Exception("Ponto inválido")
            x0 = int(valid_points[0][0])
            y0 = int(valid_points[0][1])
            x1 = int(valid_points[1][0])
            y1 = int(valid_points[1][1])
            '''
            #close to vertical
            if np.abs(theta-0)<=np.abs(theta-90) or np.abs(theta-180)<=np.abs(theta-90):
                y0 = int(0)
                y1 = int(np.shape(imgs[k])[1])
                x0 = int(r/np.cos(theta))
                x1 = int(r/np.cos(theta) - y1*np.sin(theta)/np.cos(theta))
            else: #close to horizontal
                x0 = int(0)
                x1 = int(imgs[k].shape[0])
                y0 = int(r/np.sin(theta))
                y1 = int(r/np.sin(theta) - x1*np.cos(theta)/np.sin(theta))'''
            #cv2.line(imgs[k], (x0, y0), (x1, y1), (0, 0, 255), 7)
            print((x0, y0, x1, y1))
        #print("Ponto 1: "+str((x0, y0))+" Ponto 2: "+str((x1, y1)))
        pontos[k].append(((x0, y0), (x1, y1)))
        #cv2.line(imgs[k], (x0, y0), (x1, y1), (0, 255, 0), 2)
    #print("Na imagem "+str(k)+" temos "+str(len(pontos[k]))+" pontos de linhas")
        
    solutions = []

    m_graph = Grafo(len(lines[k]))
    for i in range(len(lines[k])):
        for j in range(i+1, len(lines[k])):
            #Calculo a interseção entre as linhas
            theta = lines[k][i][0][1]
            theta_tilde = lines[k][j][0][1]
            r = lines[k][i][0][0]
            r_tilde = lines[k][j][0][0]
            if np.sin(theta-theta_tilde)!=0: #Não é singular, as linhas não são paralelas
                A = np.array([[np.cos(theta), np.sin(theta)], 
                            [np.cos(theta_tilde), np.sin(theta_tilde)]])
                b = np.array([[r], [r_tilde]])
                sol = np.linalg.inv(A)@b 
                sol = np.reshape(sol, 2)
                solutions.append(sol)
                if sol is None:
                    raise Exception("solução falha")
                if sol[0]>=0 and sol[0]<=imgs[k].shape[0] and sol[1]>=0 and sol[1]<=imgs[k].shape[1]:
                    m_graph.addEdge(i, j, sol)

    quadrilateros = []
    
    quad_lines = []

    for i in range(len(lines[k])):
        candidates = [node for node in m_graph.getNodeSet(i)]
        pares = []
        for s in range(len(candidates)):
            for t in range(s+1, len(candidates)):
                pares.append({candidates[s], candidates[t]})
        for par in pares:
            exclusion = {i}|par
            els = [elem for elem in par]
            opposites = (m_graph.getNodeSet(els[0])-exclusion)&(m_graph.getNodeSet(els[1])-exclusion) 
            for opp in opposites:
                quadrilateros.append((m_graph.getEdgeData(i,els[0]), m_graph.getEdgeData(i, els[1]), m_graph.getEdgeData(els[0], opp), m_graph.getEdgeData(els[1],opp)))
                quad_lines.append((i, els[0], els[1], opp))
            #m_graph.removeEdge(i, els[0])
            #m_graph.removeEdge(i, els[1])
    max_area = 0
    quadrilatero_max = []
    chosen_qidx = None
    for qidx, quadri in enumerate(quadrilateros):
        if is_convex(quadri):
            quadri_area = quad_area(quadri)
            quadrilatero_max = quadri if quadri_area>max_area else quadrilatero_max
            chosen_qidx = qidx if quadri_area>max_area else chosen_qidx
            max_area = quadri_area if quadri_area>max_area else max_area
    if max_area==0:
        raise NoAreasException("Was not able to find the desired areas!")
    else:
        quads.append(quadrilatero_max)
    
    #Drawing the support lines
    for linha in quad_lines[chosen_qidx]:
        cv2.line(imgs[k], pontos[k][linha][0], pontos[k][linha][1], (0, 255, 0), 1)
    
    #Drawing the quad
    cv2.line(imgs[k], (int(quads[-1][0][0]), int(quads[-1][0][1])), (int(quads[-1][1][0]), int(quads[-1][1][1])), (0, 0, 255), 2)
    cv2.line(imgs[k], (int(quads[-1][0][0]), int(quads[-1][0][1])), (int(quads[-1][2][0]), int(quads[-1][2][1])), (0, 0, 255), 2)
    cv2.line(imgs[k], (int(quads[-1][2][0]), int(quads[-1][2][1])), (int(quads[-1][3][0]), int(quads[-1][3][1])), (0, 0, 255), 2)
    cv2.line(imgs[k], (int(quads[-1][3][0]), int(quads[-1][3][1])), (int(quads[-1][1][0]), int(quads[-1][1][1])), (0, 0, 255), 2)
    
    sorted_quad = sorted(quads[-1], key = lambda elem: elem[1])
    upper_point = 0.5*(sorted_quad[0]+sorted_quad[1])
    lower_point = 0.5*(sorted_quad[2]+sorted_quad[3])
    midlines.append((upper_point, lower_point))
    cv2.line(imgs[k], (int(upper_point[0]), int(upper_point[1])), (int(lower_point[0]), int(lower_point[1])), (255, 0, 0), 6)
    
#calculate the height and step distance
comps = [math.sqrt(np.dot((midlines[0][0]-midlines[0][1]), (midlines[0][0]-midlines[0][1]))), math.sqrt(np.dot((midlines[1][0]-midlines[1][1]), (midlines[1][0]-midlines[1][1])))]
height = (delta_d/(n*((1/comps[0])-(1/comps[1]))))/pxpm
distance = delta_d*(comps[1]/(comps[1]-comps[0]))

print("Distance: "+str(distance)+" Height: "+str(height))        

cv2.imshow("janela 0", imgs[0])
cv2.imshow("janela 1", imgs[1])
            
            
cv2.waitKey(0)
cv2.destroyAllWindows()
