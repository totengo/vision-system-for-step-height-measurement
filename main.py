#TODO: correct the behavior when the distance to the camera is negative
#TODO: rasterize the hex8 after triangulating

#Parameters for test: WIREFRAME; f = 100; n = 20; x0 = -50; y0 = 0; z0 = 60; w = 300; h = 40; l = 500; vz = 0.05; target_sum = 35

import pygame
import numpy as np
import math
from scipy.linalg import expm

#I need a pespective projection matrix and a rectangular prism representing a step that the robot will measure to climb
#At each timestep I update the position and orientation of the camera and the relative positions of each vertex of the prism with 
# respect to the camera


WIDTHT, HEIGHTT = 1600, 1600
pygame.display.set_caption("Imagem da câmera")
screen = pygame.display.set_mode((WIDTHT, HEIGHTT))

WIDTH, HEIGHT = 1600, 1600

WIREFRAME = True

f = 100 # far plane distance
n = 20 # near plane distance
P_perspective = np.array([[n, 0, 0, 0], 
                          [0, n, 0, 0], 
                          [0, 0, f+n, -n*f], 
                          [0, 0, 1, 0]], np.double)

Ortho = np.array([[2.0/(WIDTH), 0, 0, 0],
                  [0, 2.0/HEIGHT, 0, 0],
                  [0, 0, 1.0/(f-n), -n/(f-n)],
                  [0, 0, 0, 1]], np.double)
P = Ortho@P_perspective
#P = Ortho
#P = P_perspective

viewMatrix = np.array([[WIDTHT/2.0, 0, 0, WIDTHT/2.0],
                       [0, HEIGHTT/2.0, 0, HEIGHTT/2.0],
                       [0, 0, 1, 0],
                       [0, 0, 0, 1]], np.double)

def to_screen(vector):
    return np.reshape(viewMatrix@np.reshape(vector, (4,1)), 4)


def toHomogeneous(vector):
    return np.array([vector[0], vector[1], vector[2], 1.0], np.double)

def toR3(vector):
    if vector[3]!=0:
        return np.array([vector[0]/vector[3], vector[1]/vector[3], vector[2]/vector[3], 1.0], np.double)
    else:
        return np.array([vector[0], vector[1], vector[2], 0.0], np.double)

x0 = -50
y0 = 0
z0 = 60
w = 300 # actual height of the step to be measured by the robot
h = 40
l = 500
vertex_list = [np.array([x0, y0, z0], np.double),
               np.array([x0+l, y0, z0], np.double),
               np.array([x0+l, y0+w, z0], np.double),
               np.array([x0, y0+w, z0], np.double),
               np.array([x0, y0+w, z0+h], np.double), 
               np.array([x0, y0, z0+h], np.double), 
               np.array([x0+l, y0, z0+h], np.double),
               np.array([x0+l, y0+w, z0+h], np.double)]
projected_points = []

edge_pairs = [(0, 1), (0, 3), (0, 5), (1, 2), (1, 6), (2, 3), (2, 7), (3, 4), (4, 5), (5, 6), (6, 7), (7, 4)]
tris_list = [(0, 1, 2), (0, 2, 3), 
             (4, 7, 6), (4, 6, 5), 
             (0, 5, 6), (0, 6, 1), 
             (2, 7, 4), (2, 4, 3), 
             (1, 6, 7), (1, 7, 2), 
             (3, 4, 5), (3, 5, 0)]

#I have declared the six faces with two triangles for each face (per face)
#The normals are calculated as (02x01)

cores = [(0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (0, 128, 128)]
normals = [(1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1)]

def render_triangle(vert1, vert2, vert3):
    diffo = vertex_list[vert3]-vertex_list[vert1]
    diffa = vertex_list[vert2]-vertex_list[vert1]
    normal = np.cross(diffo, diffa)
    normal = normal/math.sqrt(np.dot(normal, normal))
    #print(normal)
    max, max_idx = 0, 0
    for norm, idx in zip(normals, range(len(normals))):
        val = np.dot(normal, norm)
        max_idx = idx if val>max else max_idx
        max = val if val>max else max 
    cor = cores[max_idx]
    v1 = (projected_points[vert1][0], projected_points[vert1][1])
    v2 = (projected_points[vert2][0], projected_points[vert2][1])
    v3 = (projected_points[vert3][0], projected_points[vert3][1])
    pygame.draw.polygon(screen, cor, [v1, v2, v3])


first_image_saved = False
second_image_saved = False
target_sum = 35

camera_x = 0
camera_y = 0
camera_z = 0

camera_vector = np.array([camera_x, camera_y, camera_z], np.double)
delta_vector = np.array([0.0, 0.0, 0.05], np.double)

omega = np.array([0.0, 0.0, 0.0], np.double)

def omega_matrix(omega):
    return np.array([[1, omega[2], -omega[1]], 
                     [-omega[2], 1, omega[0]], 
                     [omega[1], -omega[0], 1]], np.double)

def omega_cross(omega):
    return np.array([[0, omega[2], -omega[1]], 
                     [-omega[2], 0, omega[0]], 
                     [omega[1], -omega[0], 0]], np.double)

attitude_matrix = np.eye(3)

clock = pygame.time.Clock()

sum = 0
delta_time = 0.0

fps = 60.0
stopped_sim = False

if __name__ == "__main__":
    while True:
        clock.tick(fps)
        
        screen.fill((0, 0, 0))
        
        pygame.display.set_caption("Imagem da câmera"+str(clock.get_fps()))
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    exit()
        
        projected_points = []
        
        #update the relative positions
        rel_vertex = [attitude_matrix@(np.reshape(vertex-camera_vector, (3, 1))) for vertex in vertex_list]
        rel_vertex = [np.reshape(toHomogeneous(np.reshape(vertex, 3)), (4, 1)) for vertex in rel_vertex]
        #update the camera_position
        camera_vector = camera_vector + delta_vector
        sum += math.sqrt(np.dot(delta_vector, delta_vector))
        #update the attitude_matrix
        delta_time+=1.0/fps
        print((omega[1]*delta_time/np.pi)*180.0)
        #attitude_matrix = omega_matrix(omega/fps)@attitude_matrix
        #attitude_matrix = expm(omega_cross(delta_time*omega))@np.eye(3)
        attitude_matrix = np.array([[1, 0, 0], [0, np.cos(delta_time*omega[0]), np.sin(delta_time*omega[0])], [0, -np.sin(delta_time*omega[0]), np.cos(delta_time*omega[0])]])
        print(attitude_matrix)
        printed = False
        for point in rel_vertex:
            projected = to_screen(toR3(np.reshape(P@point, 4)) )
            pygame.draw.circle(screen, (255, 0, 0), (projected[0], projected[1]), 5)
            projected_points.append(projected)
            if not printed and not stopped_sim:
                #print("original: "+str(point))
                #print("projected: "+str(projected)+" e...")
                printed = True
        
        if WIREFRAME:
            for pair in edge_pairs:
                if rel_vertex[pair[0]][2][0]>=0 and rel_vertex[pair[1]][2][0]>=0:
                    first_point = projected_points[pair[0]]
                    second_point = projected_points[pair[1]]
                    pygame.draw.line(screen, (255, 255, 255), (first_point[0], first_point[1]), (second_point[0], second_point[1]), 6)
                #print(str(first_point)+" "+str(second_point))
        else:
            for tri in tris_list:
                if rel_vertex[tri[0]][2][0]>=0 and rel_vertex[tri[1]][2][0]>=0 and rel_vertex[tri[2]][2][0]>=0:
                    render_triangle(*tri)
        pygame.display.update()
        
        if not first_image_saved:
            pygame.image.save(screen, "first_simulated_image_0.png")
            first_image_saved = True
        if not second_image_saved and sum>=target_sum:
            pygame.image.save(screen, "first_simulated_image_1.png")
            second_image_saved = True
            delta_vector = np.array([0.0, 0.0, 0.0], np.double)
            omega = np.array([0.0, 0.0, 0.0], np.double)
            stopped_sim = True
