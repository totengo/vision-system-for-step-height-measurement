class Grafo:
    def __init__(self, n=0, connections=None):
        self.n = n
        self.adj = [[set(), {}, []] for i in range(n)]
        if connections!=None:
            for c, i in zip(connections, range(len(connections))):
                self.adj[i][0] = c
    
    def addEdge(self, node1, node2, data=[]):
        self.adj[node1][0].add(node2)
        self.adj[node2][0].add(node1)
        if len(data)!=0:
            self.adj[node1][1][node2] = data
            self.adj[node2][1][node1] = data
    
    def removeEdge(self, node1, node2):
        if node1 in self.adj[node2][0]:
            self.adj[node1][0].remove(node2)
            self.adj[node2][0].remove(node1)
            if node1 in self.adj[node2][1]:
                self.adj[node1][1].pop(node2)
                self.adj[node2][1].pop(node1)
    
    def getEdgeData(self, node1, node2):
        if node1 in self.adj[node2][0] and node1 in self.adj[node2][1]:
            return self.adj[node1][1][node2]
        else:
            return None
    
    def areConnected(self, node1, node2):
        if node1 in self.adj[node2][0]:
            return True
        else:
            return False
    
    def getNodeSet(self, node):
        return self.adj[node][0]